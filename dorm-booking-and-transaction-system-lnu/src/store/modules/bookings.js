import axios from "@/config/axiosConfig";
export default {
  state: {
    bookings: [],
    payments: [],
  },
  mutations: {
    setBookings: (state, bookings) => (state.bookings = bookings),
    setPayments: (state, payments) => (state.payments = payments),
  },
  getters: {
    getBookings: (state) => state.bookings,
    getPayments: (state) => state.payments,
  },
  actions: {
    async fetchBookings({ commit }) {
      await axios
        .get(`/bookings?token=${localStorage.getItem("access_token")}`)
        .then((res) => {
          commit("setBookings", res.data);
        })
        .catch((err) => {
          console.log(err);
        });
    },
    async createBooking({ commit }, data) {
      const response = await axios
        .post(`/bookings?token=${localStorage.getItem("access_token")}`, data)
        .then((res) => {
          return res;
        })
        .catch((err) => {
          return err.response;
        });
      return response;
    },

    async updateBooking({ commit }, data) {
      var book = {
        admin_id: data.admin_id,
        room_id: data.room_id,
        student_id: data.student_id,
        date_start: data.date_start,
        date_end: data.date_end,
        amount: data.amount,
      };

      const response = await axios
        .put(
          `/bookings/${data.id}?token=${localStorage.getItem("access_token")}`,
          book
        )
        .then((res) => {
          return res;
        })
        .catch((err) => {
          return err.response;
        });
      return response;
    },

    async deleteBooking({ commit }, data) {
      const response = await axios
        .delete(
          `/bookings/${data.id}?room_id=${data.room_id}&student_id=${
            data.student_id
          }&admin_id=${data.admin_id}&token=${localStorage.getItem(
            "access_token"
          )}`
        )
        .then((res) => {
          return res;
        })
        .catch((err) => {
          return err.response;
        });
      return response;
    },
    async fetchPayments({ commit }) {
      await axios
        .get(`/payments?token=${localStorage.getItem("access_token")}`)
        .then((res) => {
          commit("setPayments", res.data);
        })
        .catch((err) => {
          console.log(err);
        });
    },
    async payBooking({ commit }, data) {
      var payment = {
        admin_id: data.admin_id,
        status: "Paid",
      };
      const response = await axios
        .put(
          `/payments/${data.id}?token=${localStorage.getItem("access_token")}`,
          payment
        )
        .then((res) => {
          return res;
        })
        .catch((err) => {
          return err.response;
        });
      return response;
    },
  },
};
