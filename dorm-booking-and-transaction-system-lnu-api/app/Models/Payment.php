<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function admin() {
        return $this->hasOne(Admin::class, 'id', 'admin_id');
    }

    public function booking() {
        return $this->belongsTo(Booking::class, 'id');
    }
}
