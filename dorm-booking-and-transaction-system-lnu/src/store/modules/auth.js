import axios from "@/config/axiosConfig";
export default {
  state: {
    token: localStorage.getItem("access_token") || "",
    user: "",
  },
  mutations: {
    setToken: (state, accessToken) => {
      localStorage.setItem("access_token", accessToken);
      state.token = accessToken;
    },
    setUser: (state, user) => (state.user = user),
    logoutUser: (state) => {
      state.token = "";
      localStorage.removeItem("access_token");
    },
  },
  getters: {
    getToken: (state) => state.token,
    getUser: (state) => state.user,
  },
  actions: {
    async signupUser({ commit }, data) {
      const response = await axios
        .post("/auth/signup", data)
        .then((res) => {
          return res;
        })
        .catch((err) => {
          return err.response;
        });

      return response;
    },
    async loginUser({ commit }, credentials) {
      const response = await axios
        .post("/auth/login", credentials)
        .then((res) => {
          commit("setToken", res.data.access_token);
          return res;
        })
        .catch((err) => {
          return err.response;
        });

      return response;
    },
    async logoutUser({ commit }) {
      const response = await axios
        .post(`/auth/logout?token=${localStorage.getItem("access_token")}`)
        .then((res) => {
          commit("logoutUser");
          return res;
        })
        .catch((err) => {
          return err.response;
        });

      return response;
    },
    async checkUser({ commit }) {
      const response = await axios
        .post(`/auth/me?token=${localStorage.getItem("access_token")}`)
        .then((res) => {
          commit("setUser", res.data);
          return res;
        })
        .catch((err) => {
          return err.response;
        });

      return response;
    },
  },
};
