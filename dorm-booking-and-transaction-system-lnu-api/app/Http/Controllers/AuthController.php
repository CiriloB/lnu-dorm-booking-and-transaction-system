<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUser;
use App\Models\Admin;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwtauth', ['except' => ['login', 'signup']]);
        \Config::set('auth.providers.users.model', \App\Models\Admin::class);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $loginField = request()->input('login');
        $credentials = null;

        if ($loginField !== null) {
            $loginType = filter_var($loginField, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
    
            request()->merge([ $loginType => $loginField ]);
    
            $credentials = request([ $loginType, 'password' ]);
        } else {
            return response()->json(['message' => 'Something wrong. Try again later.'], 500);
        }

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['message' => 'Invalid Cridentials'], 401);
        }

        return $this->respondWithToken($token);
    }

    public function signup(StoreUser $request)
    {
        //
        $admin = new Admin();
        $admin->last_name = $request->last_name;
        $admin->first_name = $request->first_name;
        $admin->middle_name = $request->middle_name;
        $admin->email = $request->email;
        $admin->username = $request->username;
        $admin->password = bcrypt($request->password);
        $admin->save();

        return response()->json(['message' => 'Account Saved']);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Verify Token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyToken() {
        return response()->json(['hasToken'=> true], 200);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'message' => 'Access Granted',
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
