<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Guardian;
use App\Models\Payment;
use App\Models\Sibling;
use App\Models\Status;
use App\Models\Student;
use Illuminate\Http\Request;

class BookRequestController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwtauth', ['except' => ['show', 'store']]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            // Student
            'guardian_full_name' => 'required',
            'guardian_address_contact' => 'required',
            'guardian_relationship' => 'required',
            'student_last_name' => 'required',
            'student_first_name' => 'required',
            'student_middle_name' => 'required',
            'student_nickname' => 'required',
            'student_birthdate' =>'required',
            'student_sex' => 'required',
            'student_weight' => 'required',
            'student_height' => 'required',
            'student_religion' =>'required',
            'student_course_section' => 'required',
            'student_address' => 'required',
            'student_contact_number' => 'required',
            'student_mother' =>'required',
            'student_mother_occupation' => 'required',
            'student_father' => 'required',
            'student_father_occupation' => 'required',
            'student_ailment_description' => 'required',
            'student_blood_type' => 'required',
            'student_allergy' => 'required',
            'student_date' => 'required',
        ]);
        
        $student = [
            'student_number' => $request->student_number,
            'last_name' => $request->student_last_name,
            'first_name' => $request->student_first_name,
            'middle_name' => $request->student_middle_name,
            'nickname' => $request->student_nickname,
            'birthdate' => $request->student_birthdate,
            'sex' => $request->student_sex,
            'weight' => $request->student_weight,
            'height' => $request->student_height,
            'religion' => $request->student_religion,
            'course_section' => $request->student_course_section,
            'address' => $request->student_address,
            'contact_number' => $request->student_contact_number,
            'telephone_number' => $request->student_telephone_number,
            'mother' => $request->student_mother,
            'mother_occupation' => $request->student_mother_occupation,
            'father' => $request->student_father,
            'father_occupation' => $request->student_father_occupation,
            'ailment_description' => $request->student_ailment_description,
            'blood_type' => $request->student_blood_type,
            'allergy' => $request->student_allergy,
            'date' => $request->student_date
        ];
        
        $student = Student::create($student);

        $guardian = [
            'student_id' => $student->id,
            'full_name' => $request->guardian_full_name,
            'address_contact' => $request->guardian_address_contact,
            'relationship' => $request->guardian_relationship,
        ];

        $guardian = Guardian::create($guardian);

        for ($i=0; $i < count($request->siblings); $i++) { 
            $sibling = [
                'student_id' => $student->id,
                'sibling_fullname_age' => $request->siblings[$i]
            ];
            Sibling::create($sibling);
        }

        $status = [
            'student_id' => $student->id,
            'status' => 'Pending'
        ];

        Status::create($status);

        return response()->json(['msg' => 'Book Requested']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($student_number)
    {

        $student = Student::where('student_number', $student_number)->first();
        
        if($student == null) {        
            return response()->json(['status' =>  'Not Found']);
        }

        //Check if has booking
        $booking = Booking::with('payment')->where('student_id', $student->id)->get();

        if($booking == null) {
            return response()->json(['status' =>  Status::where('student_id', $student->id)->pluck('status')[0], 'student_name' => $student->first_name]);
        }

        return response()->json(['status' => 'Payment', 'student_name' => $student->first_name,'bookings' => $booking]);
    }

    public function acceptRequest(Request $request) {
        $student = Student::where('id', $request->student_id)->first();
        $status = Status::where('id', $request->status_id)->first();

        $data = [
            'status' => 'Accepted'
        ];

        Status::where('id', $status->id)->where('student_id', $student->id)->update($data);

        return response()->json(['msg' => 'Booking Request Accepted']);
    }

    public function declineRequest(Request $request) {
        $student = Student::where('id', $request->student_id)->first();
        $status = Status::where('id', $request->status_id)->first();

        $data = [
            'status' => 'Declined'
        ];

        Status::where('id', $status->id)->where('student_id', $student->id)->update($data);

        return response()->json(['msg' => 'Booking Request Declined']);
    }

}
