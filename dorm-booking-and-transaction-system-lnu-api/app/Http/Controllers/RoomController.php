<?php

namespace App\Http\Controllers;

use App\Models\Room;
use Illuminate\Http\Request;

class RoomController extends Controller
{

    public function __construct()
    {
        $this->middleware('jwtauth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(Room::get());
    }

    public function availableRooms() {
        return response()->json([ 'available_rooms' => Room::where('bed_number', '>', 0)->count()]);
    }

    public function totalRooms() {
        return response()->json([ 'total_rooms' => Room::get()->count()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'room_number' => 'required',
            'room_floor' => 'required',
            'description' => 'required',
            'bed_number' => 'required',
            'price' => 'required',
     
        ]);

        
        $data = [
            'room_number' => $request->room_number,
            'room_floor' => $request->room_floor,
            'description' => $request->description,
            'bed_number' => $request->bed_number,
            'price' => $request->price,

        ];
        
        
        $room = Room::create($data);

        return response()->json(['msg' => 'Room Added', 'room' => $room]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'room_number' => 'required',
            'room_floor' => 'required',
            'description' => 'required',
            'bed_number' => 'required',
            'price' => 'required',
     
        ]);
        
        $data = [
            'room_number' => $request->room_number,
            'room_floor' => $request->room_floor,
            'description' => $request->description,
            'bed_number' => $request->bed_number,
            'price' => $request->price,

        ];
        
        Room::where('id', $id)->update($data);
        return response()->json(['msg' => 'Room Updated', 'room' => Room::where('id', $id)->get()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Room::where('id', $id)->delete();
        return response()->json(['msg' => 'Room Deleted']);
    }
}
