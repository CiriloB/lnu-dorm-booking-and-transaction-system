<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->string("student_number")->unique();
            $table->string("last_name");
            $table->string("middle_name");
            $table->string("first_name");
            $table->string("nickname");
            $table->string("birthdate");
            $table->string("religion");
            $table->string("sex");
            $table->string("height");
            $table->string("weight");
            $table->string("course_section");
            $table->string("address");
            $table->string("telephone_number")->nullable();
            $table->string("contact_number");
            $table->string("mother");
            $table->string("mother_occupation");
            $table->string("father");
            $table->string("father_occupation");
            $table->string("ailment_description");
            $table->string("blood_type");
            $table->string("allergy");
            $table->string("date");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
