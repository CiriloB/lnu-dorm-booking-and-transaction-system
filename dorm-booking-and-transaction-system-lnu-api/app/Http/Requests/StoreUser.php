<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'last_name' => 'bail|required|min:3|max:50|alpha',
            'first_name' => 'bail|required|min:3|max:50|alpha',
            'middle_name' => 'bail|required|min:3|max:50|alpha',
            'email' => 'bail|required|email|unique:users',
            'username' => 'bail|required|min:8|max:255|unique:users',
            'password' => 'bail|required|min:8|max:255'
        ];
    }

    public function messages()
    {
        return [
            //
            'last_name.required' => 'Last Name is required.',
            'last_name.min' => 'Last Name must minimum of 3 characters.',
            'last_name.max' => 'Last Name must maximum of 50 characters.',

            'first_name.required' => 'First Name is required.',
            'first_name.min' => 'First Name must minimum of 3 characters.',
            'first_name.max' => 'First Name must maximum of 50 characters.',

            'middle_name.required' => 'Middle Name is required.',
            'middle_name.min' => 'Middle Name must minimum of 3 characters.',
            'middle_name.max' => 'Middle Name must maximum of 50 characters.',

            'email.required' => 'Email Address is required.',
            'email.unique' => 'Email is already taken. Create new one.',
            'email.email' => 'Email address is invalid.',


            'username.required' => 'Username is required.',
            'username.unique' => 'Username is already taken. Create new one.',

            'password.required' => 'Password is required.',
            'password.min' => 'Password must minimum of 8 characters.',
            'password.max' => 'Password must maximum of 255 characters.',
        ];
    }
}
