<?php


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RoomController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\BookRequestController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\StudentController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([

    'middleware' => 'api',
    'prefix' => 'auth',
    'namespace' => 'App\Http\Controllers',


], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

});

Route::apiResource('rooms', RoomController::class)->only(['index', 'store', 'update', 'destroy']);
Route::get('/availablerooms', [RoomController::class, 'availableRooms']);
Route::get('/totalrooms', [RoomController::class, 'totalRooms']);
Route::apiResource('bookings', BookingController::class)->only(['index', 'store', 'update', 'destroy']);
Route::apiResource('bookrequest', BookRequestController::class)->only(['store']);
Route::get('/checkstatus/{id}', [BookRequestController::class, 'show']);
Route::put('/acceptrequest', [BookRequestController::class, 'acceptRequest']);
Route::put('/declinerequest', [BookRequestController::class, 'declineRequest']);
Route::apiResource('payments', PaymentController::class)->only(['index', 'update']);
Route::apiResource('students', StudentController::class)->only(['index']);


