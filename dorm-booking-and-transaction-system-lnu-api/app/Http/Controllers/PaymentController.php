<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwtauth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(Payment::with('admin', 'booking.student')->get());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $payment = Payment::where('id', $id)->first();

        if($payment == null) {
            response()->json(['msg' => 'Payment Not Found']);
        }
        $payment->admin_id = $request->admin_id;
        $payment->status = $request->status;
        $payment->update();

        return response()->json(['msg' => 'Booking Paid.']);

    }

}
