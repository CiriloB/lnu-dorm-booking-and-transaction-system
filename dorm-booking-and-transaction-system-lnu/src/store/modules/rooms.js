import axios from "@/config/axiosConfig";
export default {
  state: {
    rooms: [],
    totalRooms: 0,
    availableRooms: 0
  },
  mutations: {
    setRooms: (state, rooms) => (state.rooms = rooms),
    setAvailableRooms: (state, availableRooms) => (state.availableRooms = availableRooms),
    setTotalRooms: (state, totalRooms) => (state.totalRooms = totalRooms),
    setDeleteRoom: (state, id) =>
      (state.rooms = state.rooms.filter((room) => room.id !== id)),
    setCreateRoom: (state, room) => state.rooms.unshift({ ...room }),
    setUpdateRoom: (state, updatedRoom) => {
      const index = state.rooms.findIndex((room) => room.id === updatedRoom.id);
      if (index !== -1) {
        state.rooms.splice(index, 1, { ...updatedRoom });
      }
    },
  },
  getters: {
    getRooms: (state) => state.rooms,
    getAvailableRooms: (state) => state.availableRooms,
    getTotalRooms: (state) => state.totalRooms

  },
  actions: {
    async availableRooms({ commit }) {
      await axios
        .get(`/availablerooms?token=${localStorage.getItem("access_token")}`)
        .then((res) => {
          commit("setAvailableRooms", res.data.available_rooms);
        })
        .catch((err) => {
          console.log(err);
        });
    },
    async totalRooms({ commit }) {
      await axios
        .get(`/totalrooms?token=${localStorage.getItem("access_token")}`)
        .then((res) => {
          commit("setTotalRooms", res.data.total_rooms);
        })
        .catch((err) => {
          console.log(err);
        });
    },
    async fetchRooms({ commit }) {
      await axios
        .get(`/rooms?token=${localStorage.getItem("access_token")}`)
        .then((res) => {
          commit("setRooms", res.data);
        })
        .catch((err) => {
          console.log(err);
        });
    },
    async createRoom({ commit }, data) {
      const response = await axios
        .post(`/rooms?token=${localStorage.getItem("access_token")}`, data)
        .then((res) => {
          commit("setCreateRoom", res.data.room);
          return res;
        })
        .catch((err) => {
          return err.response;
        });
      return response;
    },
    async updateRoom({ commit }, data) {
      var room = {
        room_floor: data.room_floor,
        room_number: data.room_number,
        description: data.description,
        bed_number: data.bed_number,
        price: data.price,
      };
      var id = data.id;

      const response = await axios
        .put(`/rooms/${id}?token=${localStorage.getItem("access_token")}`, room)
        .then((res) => {
          commit("setUpdateRoom", res.data.room);
          return res;
        })
        .catch((err) => {
          return err.response;
        });
      return response;
    },
    async deleteRoom({ commit }, id) {
      const response = await axios
        .delete(`/rooms/${id}?token=${localStorage.getItem("access_token")}`)
        .then((res) => {
          commit("setDeleteRoom", id);
          return res;
        })
        .catch((err) => {
          return err.response;
        });
      return response;
    },
  },
};
