import axios from "@/config/axiosConfig";

export default {
  state: {
    students: [],
  },
  mutations: {
    setStudents: (state, students) => (state.students = students),
  },
  getters: {
    getStudents: (state) => state.students,
  },
  actions: {
    async fetchStudents({ commit }) {
      await axios
        .get(`/students?token=${localStorage.getItem("access_token")}`)
        .then((res) => {
          commit("setStudents", res.data);
        })
        .catch((err) => {
          console.log(err.response);
        });
    },
    async bookRequest({ commit }, data) {
      const response = await axios
        .post(`/bookrequest`, data)
        .then((res) => {
          return res;
        })
        .catch((err) => {
          return err.response;
        });
      return response;
    },

    async checkStatus({ commit }, student_number) {
      const response = await axios
        .get(`/checkstatus/${student_number}`)
        .then((res) => {
          return res;
        })
        .catch((err) => {
          return err.response;
        });
      return response;
    },

    async acceptRequest({ commit }, data) {
      const response = await axios
        .put(`/acceptrequest?token=${localStorage.getItem("access_token")}`, data)
        .then((res) => {
          return res;
        })
        .catch((err) => {
          return err.response;
        });
      return response;
    },

    async declineRequest({ commit }, data) {
      const response = await axios
        .put(`/declinerequest?token=${localStorage.getItem("access_token")}`, data)
        .then((res) => {
          return res;
        })
        .catch((err) => {
          return err.response;
        });
      return response;
    },
  },
};
