<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function student() {
        return $this->hasOne(Student::class, 'id', 'student_id');
    }

    public function admin() {
        return $this->hasOne(Admin::class, 'id', 'admin_id');
    }

    public function room() {
        return $this->belongsTo(Room::class, 'room_id');
    }

    public function payment() {
        return $this->hasOne(Payment::class, 'id');
    }
}
