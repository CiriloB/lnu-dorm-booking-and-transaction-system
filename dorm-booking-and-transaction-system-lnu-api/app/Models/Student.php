<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function guardian() {
        return $this->belongsTo(Guardian::class, 'guardian_id', 'id');
    }

    public function sibling() {
        return $this->belongsTo(Sibling::class, 'sibling_id', 'id');
    }

    public function status() {
        return $this->belongsTo(Status::class, 'id');
    }
    public function booked() {
        return $this->belongsTo(Booking::class, 'id');
    }
}
