import Vue from "vue";
import Vuex from "vuex";
import auth from "./modules/auth";
import rooms from "./modules/rooms";
import bookings from "./modules/bookings";
import students from "./modules/students";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    bookings,
    rooms,
    students,
  },
});
