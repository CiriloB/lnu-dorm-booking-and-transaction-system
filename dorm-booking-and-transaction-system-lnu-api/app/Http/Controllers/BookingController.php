<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Booking;
use App\Models\Payment;
use App\Models\Room;
use App\Models\Student;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('jwtauth');
    }

    public function index()
    {
        //
        return response()->json(Booking::with('student', 'admin', 'room')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Find Room
        $room = Room::where('id', $request->room_id)->first();
        $room->bed_number--;
        $room->update();
        
        $booking = [
            'admin_id' => $request->admin_id,
            'room_id' => $room->id,
            'student_id' => $request->student_id,
            'date_start' => $request->date_start,
            'date_end' => $request->date_end,
            'amount' => $request->amount
        ];

        $booking = Booking::create($booking);

        $payment = [
            'admin_id' => $request->admin_id,
            'booking_id' => $booking->id,
            'status' => 'Unpaid',
            'total_amount' => $request->amount
        ];

        Payment::create($payment);

        response()->json(['msg' => 'Room Booked']);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $booking = Booking::where('id', $id)->first();
        
        if($booking->room_id != $request->room_id) {
            // Old Room
            $room = Room::where('id', $booking->room_id)->first();
            $room->bed_number++;
            $room->update();
            // New Room
            $room = Room::where('id', $request->room_id)->first();
            $room->bed_number--;
            $room->update();
        }

        $booking = [
            'room_id' => $request->room_id,
            'student_id' => $request->student_id,
            'date_start' => $request->date_start,
            'date_end' => $request->date_end,
            'amount' => $request->amount
        ];

        $booking = Booking::where('id', $id)->update($booking);
        return response()->json(['msg' => 'Booking Updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
        // Find the room
        $room = Room::where('id', $request->room_id)->first();
        $room->bed_number++;
        $room->update();

        $student = Student::where('id', $request->student_id)->first();
        $admin = Admin::where('id', $request->admin_id)->first();
        Payment::where('booking_id', $id)->delete();

        Booking::where('id', $id)
            ->where('room_id',$room->id)
            ->where('student_id',$student->id)
            ->where('admin_id',$admin->id)->delete();

        return response()->json(['msg' => 'Booking Deleted']);
    }
}
