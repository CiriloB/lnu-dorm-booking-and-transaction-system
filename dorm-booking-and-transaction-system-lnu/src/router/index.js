import Vue from "vue";
import VueRouter from "vue-router";

import NotFound from "../views/NotFound.vue";
import Signup from "../views/admin/Admin/Signup.vue";
import Login from "../views/admin/Admin/Login.vue";
import AdminLandingPage from "../views/admin/Admin/LandingPage.vue";
import Home from "../views/admin/Home/Home.vue";
import Dashboard from "../views/admin/Home/Dashboard.vue";
import Rooms from "../views/admin/Home/Rooms.vue";
import Booking from "../views/admin/Home/Booking.vue";
import Transaction from "../views/admin/Home/Transaction.vue";
import Request from "../views/admin/Home/Request.vue";
import StudentLandingPage from "../views/student/LandingPage.vue";
import Book from "../views/student/Book.vue";
import Status from "../views/student/Status.vue";
import Result from "../views/student/Result.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "StudentLandingPage",
    component: StudentLandingPage,
  },
  {
    path: "/book",
    name: "Book",
    component: Book,
  },
  {
    path: "/status",
    name: "Status",
    component: Status,
  },
  {
    path: "/result",
    name: "Result",
    component: Result,
    props: true,
  },
  {
    path: "/admin",
    name: "AdminLandingPage",
    component: AdminLandingPage,
  },
  {
    path: "/admin/signup",
    name: "Signup",
    component: Signup,
  },
  {
    path: "/admin/login",
    name: "Login",
    component: Login,
  },
  {
    path: "/admin/home",
    name: "Home",
    component: Home,
    children: [
      {
        path: "dashboard",
        name: "Dashboard",
        components: {
          dashboard: Dashboard,
        },
      },
      {
        path: "booking",
        name: "Booking",
        components: {
          booking: Booking,
        },
      },
      {
        path: "request",
        name: "Request",
        components: {
          request: Request,
        },
      },
      {
        path: "rooms",
        name: "Rooms",
        components: {
          rooms: Rooms,
        },
      },
      {
        path: "transaction",
        name: "Transaction",
        components: {
          transaction: Transaction,
        },
      },
    ],
  },
  {
    path: "*",
    name: "NotFound",
    component: NotFound,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
